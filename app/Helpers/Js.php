<?php

namespace App\Helpers;

class Js
{
	private $jsList = [];

	/**
	 * @param $file
	 * @return Js
	 */
	public function add($file) {
		$this->jsList[] = $file;

		//  Adding default
		// $this->jsList[] = '/assets/js/m.js';

		return $this;
	}

	public function build() {
		$metas = view('footer/js', [
			'js'   =>  $this->jsList
		]);

		return $metas->render();
	}
}