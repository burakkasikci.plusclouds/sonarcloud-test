<?php

namespace App\Helpers;

class Css
{
	private $cssList = [];

	/**
	 * @param $file
	 * @return Css
	 */
	public function add($file) {
		$this->cssList[] = $file;

		return $this;
	}

	public function build() {
		$metas = view('header/css', [
			'css'   =>  $this->cssList
		]);

		return $metas->render();
	}
}