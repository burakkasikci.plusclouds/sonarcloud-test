<?php

namespace App\Helpers;

class Meta
{
	public static function build($customMeta) {
		$meta = [
			'locale'             => config('site.locale'),
			'ogSitename'         => config('site.og_sitename'),
			'ogImage'           =>  config('site.cdn_url') . '/index.png',
			'ogUrl'             =>  config('site.url'),
			'twitterTitle'      =>  config('site.og_sitename'),
			'twitterCard'       =>  'summary_large_image',
			'twitterImage'       => config('site.cdn_url') . '/index_twitter.png',
			'twitterSite'        => config('site.twitterSitename'),
		];

		if($customMeta) {
			$keys = array_keys($customMeta);

			for($i = 0; $i < count($keys); $i++) {
				$meta[$keys[$i]] = $customMeta[$keys[$i]];
			}
		}

		$metas = view('header/meta', $meta);

		return $metas->render();    //  Burası doğrudan HTML basıyor.
	}
}