<?php

namespace App\Helpers;

class Footer
{
	public static function build($customFooter = null) {
		return view('footer/default')->render();
	}
}