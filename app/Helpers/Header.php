<?php

namespace App\Helpers;

class Header
{
	public static function build($customHeader = null) {
		return view('header/default')->render();
	}
}