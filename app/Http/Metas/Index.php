<?php

namespace App\Http\Metas;

/**
 * Metadata of Index Page
 * @package App\Http\Metas
 */
class Index
{
	public function getMeta() {
		return [
			'pageTitle'          => '#title',
			'metaKeywords'       => '#keywords',
			'metaDescription'    => '#description',
			'metaViewport'       => 'width=device-width, initial-scale=1',
			'locale'             => 'tr',
			'ogSitename'         => 'TestSite',
			'ogTitle'            => '#title',
			'ogDescription'      => '#description',
			'ogImage'            => '/',
			'ogUrl'              => '/',
			'twitterCard'        => 'summary_large_image',
			'twitterTitle'       => 'TestSite',
			'twitterDescription' => '#description',
			'twitterImage'       => 'https://static.plusclouds.com/800310e39f739a4eccced699078ac696.png',
			'robots'             => 'all',
		];
	}
}