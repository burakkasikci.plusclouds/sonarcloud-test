<?php

namespace App\Http\Controllers;

use App\Helpers\Css;
use App\Helpers\Footer;
use App\Helpers\Header;
use App\Helpers\Js;
use App\Helpers\Meta;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class IndexController extends BaseController
{
    public function show(Request $request)
    {
	    $metaSection = Meta::build([
		    'pageTitle'          => '#title',
		    'metaKeywords'       => '#keywords',
		    'metaDescription'    => '#description',
		    'ogTitle'            => '#title',
		    'ogDescription'      => '#description',
		    'twitterDescription' => '#description',
		    'twitterImage'       => 'https://static.plusclouds.com/800310e39f739a4eccced699078ac696.png',
	    ]);

	    $css = ( new Css )
		    ->build();

	    $js = ( new Js )
		    ->add('https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js')
		    ->add('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js')
		    ->build();

	    $header      = Header::build();
	    $footer      = Footer::build();

	    $content    = view('pages/index');

        //$categories  = $this->buildCategory($webSite);

        return view('layouts/default', [
	        'css'       =>  $css,
	        'js'        =>  $js,
            'metas'    => $metaSection,
            'content'  => $content,
            //'siteMenu' => $categories,
	        'header'    =>  $header,
	        'footer'    =>  $footer
        ]);
    }
}
