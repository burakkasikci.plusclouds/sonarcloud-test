    <title>{{ $pageTitle }}</title>
    <meta name="keywords" content="{{ $metaKeywords }}">
    <meta name="description" content="{{ $metaDescription }}">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale={{ $metaViewport ?? '1' }}">

    <meta property="og:type" content="website">
    <meta property="og:locale" content="{{ $locale }}">
    <meta property="og:site_name" content="{{ $ogSitename }}">
    <meta property="og:title" content="{{ $ogTitle }}">
    <meta property="og:description" content="{{ $ogDescription }}">
    <meta property="og:image" content="{{ $ogImage }}">
    <meta property="og:url" content="{{ $ogUrl }}">

@isset($twitterCard)
    <meta name="twitter:card" content="{{ $twitterCard }}">
@endisset
@isset($twitterSite)
    <meta name="twitter:site" content="{{ $twitterSite }}">
@endisset
    <meta name="twitter:title" content="{{ $twitterTitle }}">
@isset($twitterDescription)
    <meta name="twitter:description" content="{{ $twitterDescription }}">
@endisset
@isset($twitterImage)
    <meta property="twitter:image" content="{{ $twitterImage }}">
@endisset

@isset($robots)
    <meta name="robots" content="{{ $robots }}">
@endisset

    <link rel="icon" type="image/x-icon" href="https://static.plusclouds.com/static/favicon.png">