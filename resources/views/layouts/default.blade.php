<!DOCTYPE html>
<html lang="en" xml:lang="en">
<head lang="{!! App::getLocale() !!}">
    <meta charset="utf-8" />
    {!! $metas !!}
    {!! $css !!}
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
{!! $header !!}
{!! $content !!}
{!! $footer !!}
{!! $js !!}
</body>
</html>